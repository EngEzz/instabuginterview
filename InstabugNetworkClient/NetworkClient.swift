//
//  NetworkClient.swift
//  InstabugNetworkClient
//
//  Created by Yousef Hamza on 1/13/21.
//

import Foundation

import CoreData

public class NetworkClient {
    public static var shared = NetworkClient()
    
    let maximumSavedNumber = 1000
    let maximumPayloadSize: Double = 1
    let defaultPayload = "payload too large"
    let queue = DispatchQueue(label: "serial")
    
    var persistentContainer: NSPersistentContainer = {
        let coreDataModelName = "CoreDataModel"
        let bundle = Bundle(for: InstabugNetworkClient.NetworkClient.self )
        let modelURL = bundle.url(forResource: coreDataModelName, withExtension: "momd")!
        let managedObjectModel =  NSManagedObjectModel(contentsOf: modelURL)
        let container = NSPersistentContainer(name: coreDataModelName, managedObjectModel: managedObjectModel!)
        container.loadPersistentStores { (storeDescription, error) in
            
            if let err = error {
                fatalError("❌ Loading of store failed:\(err)")
            }
        }
        container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return container
    }()
    
    // MARK: Network requests
    public func get(_ url: URL, completionHandler: @escaping (Data?) -> Void) {
        executeRequest(url, method: "GET", payload: nil, completionHandler: completionHandler)
    }

    public func post(_ url: URL, payload: Data?=nil, completionHandler: @escaping (Data?) -> Void) {
        executeRequest(url, method: "POSt", payload: payload, completionHandler: completionHandler)
    }

    public func put(_ url: URL, payload: Data?=nil, completionHandler: @escaping (Data?) -> Void) {
        executeRequest(url, method: "PUT", payload: payload, completionHandler: completionHandler)
    }

    public func delete(_ url: URL, completionHandler: @escaping (Data?) -> Void) {
        executeRequest(url, method: "DELETE", payload: nil, completionHandler: completionHandler)
    }

    func executeRequest(_ url: URL, method: String, payload: Data?, completionHandler: @escaping (Data?) -> Void) {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method
        urlRequest.httpBody = payload
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            self.queue.async(flags:.barrier) {
                self.startLogging(urlRequest: urlRequest, data: data, response: response, error: error)
            }
            DispatchQueue.main.async {
                completionHandler(data)
            }
        }.resume()
    }

    // MARK: Network recording
    func startLogging(urlRequest: URLRequest, data: Data?, response: URLResponse?, error: Error?) {
        let context = persistentContainer.newBackgroundContext()
        context.performAndWait {
            let request = NetworkRequest(context: context)
            if let error = error as NSError? {
                request.errorCode = Int16(error.code)
                request.errorDomain = error.domain
            } else if let httpResponse = response as? HTTPURLResponse {
                request.statusCode = Int16(httpResponse.statusCode)
                request.responsePayload = self.getPayloadData(payloadData: data)
            }
            
            request.url = urlRequest.url?.description
            request.httpMethod = urlRequest.httpMethod
            request.requestPayload = self.getPayloadData(payloadData: urlRequest.httpBody)
            self.saveRequest(context: context)
        }
    }
    
    func getPayloadData(payloadData: Data?)-> Data? {
        let dataCount = payloadData?.count ?? 0
        let byteSize = Measurement(value: Double(dataCount), unit: UnitInformationStorage.bytes)
        let convertedSize = byteSize.converted(to: .megabytes)
        if convertedSize.value > maximumPayloadSize {
            return defaultPayload.data(using: .utf8)
        } else {
            return payloadData
        }
    }
    
    func saveRequest(context: NSManagedObjectContext) {
        do {
            fetchRequests(context: context, complete: {requests in
                if (requests?.count ?? 0) > self.maximumSavedNumber {
                    if let firstRecord = requests?.first {
                        firstRecord.managedObjectContext!.delete(firstRecord)
                    }
                }
            })
            try context.save()
        } catch let error {
            print("❌ Failed to create request: \(error.localizedDescription)")
        }
    }

    public func deleteAllData() {
        let context = persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "\(NetworkRequest.self)")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch let error {
            print("❌ Failed to delete all requests: \(error.localizedDescription)")
        }
    }
    
    func fetchRequests(context: NSManagedObjectContext,complete:@escaping (([NetworkRequest]?)->Void)) {
        context.performAndWait {
            let fetchRequest = NSFetchRequest<NetworkRequest>(entityName: "\(NetworkRequest.self)")
            do {
                let requests = try context.fetch(fetchRequest)
                complete(requests)
            } catch let error {
                print("❌ Failed to fetch all requests: \(error.localizedDescription)")
                complete(nil)
            }
        }
    }
    
    public func allNetworkRequests(complete: @escaping (([NetworkRequest]?)->Void)) {
        self.queue.sync (execute: {
            self.fetchRequests(context:self.persistentContainer.newBackgroundContext(),complete: {requests in
                complete(requests)
            })
        })
    }
}
