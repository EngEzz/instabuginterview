//
//  InstabugNetworkClientTests.swift
//  InstabugNetworkClientTests
//
//  Created by Yousef Hamza on 1/13/21.
//

import XCTest
@testable import InstabugNetworkClient

class InstabugNetworkClientTests: XCTestCase {

    override func setUpWithError() throws {
        
    }
    //MARK: - GetRequest_onAPISuccess_onSuccessReqsponseSaved
    func test_GetRequest_onAPISuccess_onSuccessRequestSaved() {
        // Given
        let urlPath = "https://httpbin.org/bytes/100"
        let httpMethod = "GET"
        let statusCode = "200"
        let url = URL(string: urlPath)
        var savedRequest: NetworkRequest?
        let didReceiveResponse = expectation(description: "")
        // When
        NetworkClient.shared.get(url!, completionHandler: {data in
                NetworkClient.shared.allNetworkRequests(complete: { result in
                    savedRequest = result?.filter{$0.url == urlPath && $0.httpMethod == httpMethod && $0.statusCode.description == statusCode}.first
                    didReceiveResponse.fulfill()
                })
        })
        wait(for: [didReceiveResponse], timeout: .greatestFiniteMagnitude)
        // Then
        XCTAssertNotNil(savedRequest)
    }
    
    //MARK: - GetRequest_onAPIFailure_onFailureReqsponseSaved
    func test_GetRequest_onAPIFailure_onFailureReqsponseSaved() {
        // Given
        let urlPath = "https:"
        let httpMethod = "GET"
        let errorCode = "-1000"
        let errorDomain = "NSURLErrorDomain"
        let url = URL(string: urlPath)
        var savedRequest: NetworkRequest?
        let didReceiveResponse = expectation(description: "")
        // When
        NetworkClient.shared.get(url!, completionHandler: {data in
                NetworkClient.shared.allNetworkRequests(complete: { result in
                    savedRequest = result?.filter{$0.url == urlPath && $0.httpMethod == httpMethod && $0.errorCode.description == errorCode && $0.errorDomain == errorDomain}.first
                    didReceiveResponse.fulfill()
                })
        })
        wait(for: [didReceiveResponse], timeout: .greatestFiniteMagnitude)
        // Then
        XCTAssertNotNil(savedRequest)
    }
    
    //MARK: - PostRequest_onAPISuccess_onSuccessReqsponseSaved
    func test_PostRequest_onAPISuccess_onSuccessRequestSaved() {
        // Given
        let urlPath = "https://httpbin.org/post"
        let httpMethod = "POST"
        let statusCode = "200"
        let url = URL(string: urlPath)
        var savedRequest: NetworkRequest?
        let didReceiveResponse = expectation(description: "")
        // When
        NetworkClient.shared.post(url!, completionHandler: { data in
            NetworkClient.shared.allNetworkRequests(complete: { result in
                savedRequest = result?.filter{$0.url == urlPath && $0.httpMethod == httpMethod && $0.statusCode.description == statusCode}.first
                didReceiveResponse.fulfill()
            })
        })
        wait(for: [didReceiveResponse], timeout: .greatestFiniteMagnitude)
        // Then
        XCTAssertNotNil(savedRequest)
    }
    
    //MARK: - PostRequest_onAPIFailure_onFailureReqsponseSaved
    func test_PostRequest_onAPIFailure_onFailureReqsponseSaved() {
        // Given
        let urlPath = "https:"
        let httpMethod = "POST"
        let errorCode = "-1000"
        let errorDomain = "NSURLErrorDomain"
        let url = URL(string: urlPath)
        var savedRequest: NetworkRequest?
        let didReceiveResponse = expectation(description: "")
        // When
        NetworkClient.shared.post(url!, completionHandler: { data in
            NetworkClient.shared.allNetworkRequests(complete: { result in
                savedRequest = result?.filter{$0.url == urlPath && $0.httpMethod == httpMethod && $0.errorCode.description == errorCode && $0.errorDomain == errorDomain}.first
                didReceiveResponse.fulfill()
            })
        })
        wait(for: [didReceiveResponse], timeout: .greatestFiniteMagnitude)
        // Then
        XCTAssertNotNil(savedRequest)
    }
    
    //MARK: - PostRequest_onAPISuccessPayloadMoreThanOneMG_onSuccessReqsponseSavedWithDefaultPayload
    func test_PostRequest_onAPISuccessPayloadMoreThanOneMG_onSuccessReqsponseSavedWithDefaultPayload() {
        // Given
        let bundle = Bundle(for: type(of: self))
        let path = bundle.path(forResource: "testImage", ofType: "jpg")
        let imageData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""))
        let urlPath = "https://httpbin.org/post"
        let httpMethod = "POST"
        let statusCode = "200"
        let url = URL(string: urlPath)
        let defaultPayloadData = "payload too large".data(using: .utf8)
        let didReceiveResponse = expectation(description: "")
        var savedRequest: NetworkRequest?
        // When
        NetworkClient.shared.post(url!,payload: imageData, completionHandler: { data in
            NetworkClient.shared.allNetworkRequests(complete: { result in
                savedRequest = result?.filter{$0.url == urlPath && $0.httpMethod == httpMethod && $0.statusCode.description == statusCode && $0.responsePayload == defaultPayloadData && $0.requestPayload == defaultPayloadData}.first
                didReceiveResponse.fulfill()
            })
        })
        wait(for: [didReceiveResponse], timeout: .greatestFiniteMagnitude)
        // Then
        XCTAssertNotNil(savedRequest)
    }
    
    //MARK: - DeleteRequest_onAPISuccess_onSuccessReqsponseSaved
    func test_DeleteRequest_onAPISuccess_onSuccessRequestSaved() {
        // Given
        let urlPath = "https://httpbin.org/delete"
        let httpMethod = "DELETE"
        let statusCode = "200"
        let url = URL(string: urlPath)
        var savedRequest: NetworkRequest?
        let didReceiveResponse = expectation(description: "")
        // When
        NetworkClient.shared.delete(url!, completionHandler: { data in
            NetworkClient.shared.allNetworkRequests(complete: { result in
                savedRequest = result?.filter{$0.url == urlPath && $0.httpMethod == httpMethod && $0.statusCode.description == statusCode}.first
                didReceiveResponse.fulfill()
            })
        })
        wait(for: [didReceiveResponse], timeout: .greatestFiniteMagnitude)
        // Then
        XCTAssertNotNil(savedRequest)
    }
    
    //MARK: - DeleteRequest_onAPIFailure_onFailureReqsponseSaved
    func test_DeleteRequest_onAPIFailure_onFailureReqsponseSaved() {
        // Given
        let urlPath = "https:"
        let httpMethod = "DELETE"
        let errorCode = "-1000"
        let errorDomain = "NSURLErrorDomain"
        let url = URL(string: urlPath)
        var savedRequest: NetworkRequest?
        let didReceiveResponse = expectation(description: "")
        // When
        NetworkClient.shared.delete(url!, completionHandler: { data in
            NetworkClient.shared.allNetworkRequests(complete: { result in
                savedRequest = result?.filter{$0.url == urlPath && $0.httpMethod == httpMethod && $0.errorCode.description == errorCode && $0.errorDomain == errorDomain}.first
                didReceiveResponse.fulfill()
            })
        })
        wait(for: [didReceiveResponse], timeout: .greatestFiniteMagnitude)
        // Then
        XCTAssertNotNil(savedRequest)
    }
    
    //MARK: - PutRequest_onAPISuccess_onSuccessReqsponseSaved
    func test_PutRequest_onAPISuccess_onSuccessRequestSaved() {
        // Given
        let urlPath = "https://httpbin.org/put"
        let httpMethod = "PUT"
        let statusCode = "200"
        let url = URL(string: urlPath)
        var savedRequest: NetworkRequest?
        let didReceiveResponse = expectation(description: "")
        // When
        NetworkClient.shared.put(url!, completionHandler: { data in
            NetworkClient.shared.allNetworkRequests(complete: { result in
                savedRequest = result?.filter{$0.url == urlPath && $0.httpMethod == httpMethod && $0.statusCode.description == statusCode}.first
                didReceiveResponse.fulfill()
            })
        })
        wait(for: [didReceiveResponse], timeout: .greatestFiniteMagnitude)
        // Then
        XCTAssertNotNil(savedRequest)
    }
    
    //MARK: - PutRequest_onAPIFailure_onFailureReqsponseSaved
    func test_PutRequest_onAPIFailure_onFailureReqsponseSaved() {
        // Given
        let urlPath = "https:"
        let httpMethod = "PUT"
        let errorCode = "-1000"
        let errorDomain = "NSURLErrorDomain"
        let url = URL(string: urlPath)
        var savedRequest: NetworkRequest?
        let didReceiveResponse = expectation(description: "")
        // When
        NetworkClient.shared.put(url!, completionHandler: { data in
            NetworkClient.shared.allNetworkRequests(complete: { result in
                savedRequest = result?.filter{$0.url == urlPath && $0.httpMethod == httpMethod && $0.errorCode.description == errorCode && $0.errorDomain == errorDomain}.first
                didReceiveResponse.fulfill()
            })
        })
        wait(for: [didReceiveResponse], timeout: .greatestFiniteMagnitude)
        // Then
        XCTAssertNotNil(savedRequest)
    }
    
    //MARK: - GetRequest_onSavedRequestExceedsMaximum_onFirstRequestRemoved
    func test_GetRequest_onSavedRequestExceedsMaximum_onFirstRequestRemoved() {
        // Given
        let firstUrlPath = "https:first"
        let urlPath = "https:"
        let firstUrl = URL(string: firstUrlPath)
        let url = URL(string: urlPath)
        var isFirstRequestRemoved: Bool = false
        let didReceiveResponse = expectation(description: "")
        let dispatchGroup = DispatchGroup()
        let maximumNumber = 10001
        
        for index in 0..<maximumNumber {
            dispatchGroup.enter()
            let url = index == .zero ? firstUrl : url
            NetworkClient.shared.get(url!, completionHandler: { data in
                dispatchGroup.leave()
            })
        }
        dispatchGroup.notify(queue: .main) {
            NetworkClient.shared.allNetworkRequests(complete: { result in
                isFirstRequestRemoved = result?.first?.url != firstUrlPath
                didReceiveResponse.fulfill()
            })
        }
        // When
        
        wait(for: [didReceiveResponse], timeout: .greatestFiniteMagnitude)
        // Then
        XCTAssertTrue(isFirstRequestRemoved)
    }
}
